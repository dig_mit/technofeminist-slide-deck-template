# Technofeminist Slide Deck Template

This is a template for a slide deck using [reveal.js](https://revealjs.com/)

To use it, clone the repo and put a current version of reveal in the `reveal.js`
folder (or link to an existing one under the same web root, if you use this
more often). Then adapt the index.html and slides.html files as needed.

TODO:
- more infos on usage
- license
