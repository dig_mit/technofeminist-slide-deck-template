let title = document.getElementById('cover-slide-title')
let coverSlideInterval
let coverSlideIntervalTime = 30

let titleX = 0
let titleY = 0
let diffX = -2
let diffY = -1
let maxX = window.innerWidth / 6

function moveTitle () {
  titleX = titleX + diffX
  titleY = titleY + diffY
  if (titleX < -maxX || titleX > maxX) {diffX = -diffX}
  if (titleY < -30 || titleY > 230) {diffY = -diffY}
  title.style.left = titleX + 'px'
  title.style.top = titleY + 'px'
}

coverSlideInterval = setInterval(moveTitle, coverSlideIntervalTime)

title.onclick = () => {
  if (coverSlideInterval) {
    clearInterval(coverSlideInterval)
    coverSlideInterval = false
  } else {
    coverSlideInterval = setInterval(moveTitle, coverSlideIntervalTime)
  }
}